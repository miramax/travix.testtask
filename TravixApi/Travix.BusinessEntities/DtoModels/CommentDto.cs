﻿namespace Travix.BusinessEntities.DtoModels
{
    public class CommentDto
    {
        public int Id { get; set; }
        public string Content { get; set; }
        public PostDto Post { get; set; }
    }
}

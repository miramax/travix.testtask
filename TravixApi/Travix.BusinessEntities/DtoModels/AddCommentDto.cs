﻿namespace Travix.BusinessEntities.DtoModels
{
    public class AddCommentDto
    {
        public int PostId { get; set; }
        public string Content { get; set; }
    }
}

﻿using System.Collections.Generic;

namespace Travix.BusinessEntities.DtoModels
{
    public class PostDto
    {
        public int Id { get; set; }
        public string Content { get; set; }
        public virtual ICollection<CommentDto> Comments { get; set; }
    }
}

﻿using System;

namespace Travix.BusinessEntities.Exceptions
{
    public class MessageException : Exception
    {
        public MessageException(string message) : base(message)
        {
        }
    }
}

﻿using Microsoft.AspNetCore.Identity;

namespace Travix.BusinessEntities.DataContracts
{
    public class ApplicationUser : IdentityUser
    {
    }
}

﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Travix.BusinessEntities.DataContracts
{
    public sealed class Post
    {
        public Post()
        {
            Comments = new HashSet<Comment>();
        }
        
        public int Id { get; set; }
        [Required]
        public string Content { get; set; }
        public ICollection<Comment> Comments { get; set; }
    }
}

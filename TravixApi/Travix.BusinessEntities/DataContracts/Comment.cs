﻿using System.ComponentModel.DataAnnotations;

namespace Travix.BusinessEntities.DataContracts
{
    public class Comment
    {
        public int Id { get; set; }
        [Required]
        public string Content { get; set; }
        [Required]
        public int PostId { get; set; }
        public virtual Post Post { get; set; }
    }
}

﻿namespace Travix.BusinessEntities.DataContracts
{
    public class AddComment
    {
        public int PostId { get; set; }
        public string Content { get; set; }
    }
}

﻿using AutoMapper;
using Travix.BusinessEntities.DataContracts;
using Travix.BusinessEntities.DtoModels;

namespace Travix.Infrastructure.Mapping.Profiles
{
    public class BusinessEntitiesMappingProfile : Profile
    {
        public BusinessEntitiesMappingProfile()
        {
            CreateMap<CommentDto, Comment>().ReverseMap();
            CreateMap<PostDto, Post>().ReverseMap();
            CreateMap<AddCommentDto, AddComment>();
            CreateMap<AddCommentDto, CommentDto>();
        }
    }
}

﻿using Autofac;
using Autofac.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.IO;
using AutoMapper;
using Travix.Api.MappingProfiles;
using Travix.BusinessComponents.Configuration;
using Travix.DataAccess;
using Travix.DataAccess.Seed;
using Travix.DataAccess.Seed.SqlCommands;
using Travix.Infrastructure.Mapping.Profiles;

namespace Travix.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        private void SeedDb()
        {
            using (var db = new ApplicationDbContext())
            {
                var commands = new TravixTestTaskCreateDatabaseIfNotExists(
                    new InsertPostWithRandomContent(),
                    new InsertCommentsToFirstPost());
                commands.Seed(db);
            }
        }

        private void LoadConfiguration()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", false, true);
            IConfigurationRoot root = builder.Build();
            ConfigurationManager.ConnectionString = root.GetConnectionString("DefaultConnection");
        }

        public AutofacServiceProvider GetAutofacConfiguredProvider(IServiceCollection services)
        {
            var containerBuilder = new ContainerBuilder();
            containerBuilder.RegisterModule<TravixApiModule>();
            containerBuilder.Populate(services);
            var container = containerBuilder.Build();

            return new AutofacServiceProvider(container);
        }

        private static void AddApplicationServices(IServiceCollection services)
        {
            services
                .AddSeedCommands()
                .AddDataAccessServices()
                .AddBusinessComponents()
                .AddControllers();
        }

        public static void ConfigureMapping(IServiceCollection services)
        {
            var mapperConfig = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new TravixApiMapping());
                cfg.AddProfile(new BusinessEntitiesMappingProfile());
            });

            var mapper = mapperConfig.CreateMapper();
            
            services.AddSingleton(mapper);
        }

        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
            AddApplicationServices(services);
            ConfigureMapping(services);

            return GetAutofacConfiguredProvider(services);
        }

        private void ConfigureRoutes(IApplicationBuilder app)
        {
            app.UseMvc();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            LoadConfiguration();
            ConfigureRoutes(app);
            SeedDb();
        }
    }
}

﻿using System.ComponentModel.DataAnnotations;

namespace Travix.Api.ViewModels
{
    public class AddCommentViewModel
    {
        [Required]
        public int PostId { get; set; }

        [Required]
        [MaxLength(20)]
        public string Content { get; set; }
    }
}

﻿namespace Travix.Api.ViewModels
{
    public class PostViewModel
    {
        public int Id { get; set; }
        public string Content { get; set; }
    }
}

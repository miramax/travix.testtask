﻿using System.ComponentModel.DataAnnotations;

namespace Travix.Api.ViewModels
{
    public class AddPostViewModel
    {
        [Required]
        [MaxLength(20)]
        public string Content { get; set; }
    }
}
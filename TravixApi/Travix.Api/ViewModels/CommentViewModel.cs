﻿namespace Travix.Api.ViewModels
{
    public class CommentViewModel
    {
        public int Id { get; set; }
        public string Content { get; set; }
        public PostViewModel Post { get; set; }
    }
}

﻿using AutoMapper;
using Travix.Api.ViewModels;
using Travix.BusinessEntities.DtoModels;

namespace Travix.Api.MappingProfiles
{
    public class TravixApiMapping : Profile
    {
        public TravixApiMapping()
        {
            CreateMap<CommentViewModel, CommentDto>().ReverseMap();
            CreateMap<PostViewModel, PostDto>().ReverseMap();
            CreateMap<AddPostViewModel, PostDto>();
            CreateMap<AddCommentViewModel, CommentDto>();
            CreateMap<AddCommentViewModel, AddCommentDto>();
        }
    }
}

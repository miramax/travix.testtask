﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Travix.Api.ViewModels;
using Travix.BusinessComponents.Interfaces.Public;
using Travix.BusinessEntities.DtoModels;
using Travix.BusinessEntities.Exceptions;

namespace Travix.Api.Controllers
{
    /// <summary>
    ///Allows to see all comments, to add new comment and to get comments by id.
    /// </summary>
    /// <seealso cref="Microsoft.AspNetCore.Mvc.Controller" />
    [Route("api/v1/comments")]
    public class CommentsController : Controller
    {
        private readonly ICommentsService _commentsService;
        private readonly IMapper _mapper;
        public CommentsController(ICommentsService commentsService, IMapper mapper)
        {
            _commentsService = commentsService;
            _mapper = mapper;
        }

        /// <summary>
        /// Returns the list of all comments.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("all")]
        public async Task<IActionResult> GetAll()
        {
            var comments = await _commentsService.GetAllComments();
            var result = _mapper.Map<IEnumerable<CommentViewModel>>(comments);

            return Ok(result);
        }

        /// <summary>
        /// Adds the specified model.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("add")]
        public async Task<IActionResult> Add([FromBody]AddCommentViewModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var addCommentDto = _mapper.Map<AddCommentDto>(model);

            try
            {
                var comment = await _commentsService.AddComment(addCommentDto);
                var result = _mapper.Map<CommentViewModel>(comment);
                return CreatedAtAction("Get", new { id = comment.Id }, result);
            }
            catch (MessageException)
            {
                return BadRequest(new { message = "Bad content message" });
            }
        }

        /// <summary>
        /// Gets the specified post identifier.
        /// </summary>
        /// <param name="postId">The post identifier.</param>
        /// <param name="commentId">The comment identifier.</param>
        /// <returns></returns>
        [HttpGet]
        [Route("get")]
        public async Task<IActionResult> Get(int postId, int commentId)
        {
            var model = await _commentsService.Get(postId, commentId);
            var result = _mapper.Map<CommentViewModel>(model);

            return Ok(result);
        }
    }
}
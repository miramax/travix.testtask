﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Travix.Api.ViewModels;
using Travix.BusinessComponents.Interfaces.Public;
using Travix.BusinessEntities.DtoModels;
using Travix.BusinessEntities.Exceptions;

namespace Travix.Api.Controllers
{
    /// <summary>
    /// Allows to see all posts, to add new post and to get posts by id.
    /// </summary>
    /// <seealso cref="Microsoft.AspNetCore.Mvc.Controller" />
    [Route("api/v1/posts")]
    public class PostsController : Controller
    {
        private readonly IPostsService _postsService;
        private readonly IMapper _mapper;
        public PostsController(IPostsService postsService, IMapper mapper)
        {
            _postsService = postsService;
            _mapper = mapper;
        }

        /// <summary>
        /// Returns the list of all posts.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("all")]
        public async Task<IActionResult> GetAll()
        {
            var posts = await _postsService.GetAllPosts();
            var result = _mapper.Map<IEnumerable<PostViewModel>>(posts);

            return Ok(result);
        }

        /// <summary>
        /// Adds the post. Data have to be passed through the body.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("add")]
        public async Task<IActionResult> Add([FromBody]AddPostViewModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var postDto = _mapper.Map<PostDto>(model);

            try
            {
                var post = await _postsService.AddPost(postDto);
                var result = _mapper.Map<PostViewModel>(post);
                return CreatedAtAction("Get", new { id = postDto.Id }, result);
            }
            catch (MessageException)
            {
                return BadRequest(new { message = "Bad content message" });
            }
        }

        /// <summary>
        /// Returns the specific post by Id.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="withComments">if set to <c>true</c> [with comments].</param>
        /// <returns></returns>
        [HttpGet]
        [Route("get")]
        public async Task<IActionResult> Get(int id, bool withComments = false)
        {
            var model = await _postsService.Get(id, withComments);
            var result = _mapper.Map<PostViewModel>(model);

            return Ok(result);
        }
    }
}
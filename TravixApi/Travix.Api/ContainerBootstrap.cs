﻿using Microsoft.Extensions.DependencyInjection;
using Travix.Api.Controllers;
using Travix.BusinessComponents.Interfaces.Database;
using Travix.BusinessComponents.Interfaces.Public;
using Travix.BusinessComponents.Services;
using Travix.DataAccess.Seed.SqlCommands;
using Travix.DataAccess.Services;

namespace Travix.Api
{
    /// <summary>
    /// Container configuration
    /// </summary>
    public static class ContainerBootstrap
    {
        /// <summary>
        /// Controllers configurations. Trancient lifetime for every controller.
        /// Creates new controller per access.
        /// </summary>
        /// <param name="services">The services.</param>
        /// <returns></returns>
        public static IServiceCollection AddControllers(this IServiceCollection services)
        {
            services
                .AddTransient<CommentsController>()
                .AddTransient<PostsController>();
 
            return services;
        }

        /// <summary>
        /// Business components registration.
        /// </summary>
        /// <param name="services">The services.</param>
        /// <returns></returns>
        public static IServiceCollection AddBusinessComponents(this IServiceCollection services)
        {
            services
                .RegisterType<ICommentsService, CommentsService>()
                .RegisterType<IPostsService, PostsService>();

            return services;
        }

        public static IServiceCollection AddDataAccessServices(this IServiceCollection services)
        {
            services
                .RegisterType<ICommentsDatabaseService, CommentsDatabaseService>()
                .RegisterType<IPostsDatabaseService, PostsDatabaseService>();

            return services;
        }

        /// <summary>
        /// Seed commands registration.
        /// </summary>
        /// <param name="services">The services.</param>
        /// <returns></returns>
        public static IServiceCollection AddSeedCommands(this IServiceCollection services)
        {
            services
                .AddTransient<InsertCommentsToFirstPost>()
                .AddTransient<InsertPostWithRandomContent>();

            return services;
        }

        /// <summary>
        /// Registers the type as object per request.
        /// </summary>
        /// <typeparam name="TService">The type of the service.</typeparam>
        /// <typeparam name="TImplementation">The type of the implementation.</typeparam>
        /// <param name="services">The services.</param>
        /// <returns></returns>
        private static IServiceCollection RegisterType<TService, TImplementation>(this IServiceCollection services)
            where TService : class
            where TImplementation : class, TService
        {
            return services.AddScoped<TService, TImplementation>();
        }
    }
}

﻿using Autofac;
using Travix.DataAccess;
using Travix.DataAccess.Seed;

namespace Travix.Api
{
    public class TravixApiModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder
                .RegisterType<TravixTestTaskCreateDatabaseIfNotExists>()
                .As<TravixTestTaskCreateDatabaseIfNotExists>();

            builder
                .RegisterType<ApplicationDbContext>()
                .AsImplementedInterfaces()
                .As<ApplicationDbContext>()
                .InstancePerLifetimeScope();
        }
    }
}

﻿using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using FluentAssertions;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Travix.Api;
using Travix.Api.ViewModels;
using Xunit;

namespace Travix.Tests.Controllers
{
    public class CommentsControllerIntegrationTest
    {
        private readonly HttpClient _client;

        public CommentsControllerIntegrationTest()
        {
            var server = new TestServer(new WebHostBuilder()
                .UseConfiguration(new ConfigurationBuilder()
                    .AddJsonFile("appsettings.json")
                    .Build()
                )
                .UseStartup<Startup>());
            _client = server.CreateClient();
        }


        [Fact]
        public async Task Add_ShouldReturnsBadRequestBecauseOfEmptyRequredField()
        {
            var newComment = new AddCommentViewModel();

            var content = JsonConvert.SerializeObject(newComment);
            var stringContent = new StringContent(content, Encoding.UTF8, "application/json");

            var response = await _client.PostAsync("api/v1/posts/add", stringContent);

            response.StatusCode.Should().Be(System.Net.HttpStatusCode.BadRequest);
        }

        [Fact]
        public async Task Add_ShouldReturnsBadRequestBecauseOfBadMessage()
        {
            var newComment = new AddCommentViewModel
            {
                Content = "!@#$%^&()"
            };

            var content = JsonConvert.SerializeObject(newComment);
            var stringContent = new StringContent(content, Encoding.UTF8, "application/json");

            var response = await _client.PostAsync("api/v1/posts/add", stringContent);

            response.StatusCode.Should().Be(System.Net.HttpStatusCode.BadRequest);

            var responseString = await response.Content.ReadAsStringAsync();
            responseString.Should().Contain("Bad content message");
        }
    }
}

﻿using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using FluentAssertions;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Travix.Api;
using Travix.Api.ViewModels;
using Xunit;

namespace Travix.Tests.Controllers
{
    public class PostControllerIntegrationTest
    {
        private readonly HttpClient _client;

        public PostControllerIntegrationTest()
        {
            var server = new TestServer(new WebHostBuilder()
                .UseConfiguration(new ConfigurationBuilder()
                    .AddJsonFile("appsettings.json")
                    .Build()
                )
                .UseStartup<Startup>());
            _client = server.CreateClient();
        }


        [Fact]
        public async Task Add_ShouldReturnsBadRequestBecauseOfMaxLengthExcessing()
        {
            var newPost = new AddPostViewModel
            {
                Content = "12345678901234567890123456"
            };

            var content = JsonConvert.SerializeObject(newPost);
            var stringContent = new StringContent(content, Encoding.UTF8, "application/json");

            var response = await _client.PostAsync("api/v1/posts/add", stringContent);

            response.StatusCode.Should().Be(System.Net.HttpStatusCode.BadRequest);
        }

        [Fact]
        public async Task Add_ShouldReturnsBadRequestBecauseOfBadMessage()
        {
            var newPost = new AddPostViewModel
            {
                Content = "!@#$%^&()"
            };

            var content = JsonConvert.SerializeObject(newPost);
            var stringContent = new StringContent(content, Encoding.UTF8, "application/json");

            var response = await _client.PostAsync("api/v1/posts/add", stringContent);

            response.StatusCode.Should().Be(System.Net.HttpStatusCode.BadRequest);

            var responseString = await response.Content.ReadAsStringAsync();
            responseString.Should().Contain("Bad content message");
        }
    }
}

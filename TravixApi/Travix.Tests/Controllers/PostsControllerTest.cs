﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Travix.Api.Controllers;
using Travix.Api.ViewModels;
using Travix.BusinessComponents.Interfaces.Public;
using Travix.BusinessEntities.DtoModels;
using Xunit;

namespace Travix.Tests.Controllers
{
    public class PostsControllerTest
    {
        private readonly Mock<IPostsService> _postsServiceMock = new Mock<IPostsService>();
        private readonly Mock<IMapper> _mapperMock = new Mock<IMapper>();
        private const int TestId = 1993;

        public PostsControllerTest()
        {
            // GetAll action configurartion

            _postsServiceMock.Setup(repo => repo.GetAllPosts()).Returns(Task.FromResult(GetTestPosts()));
            _mapperMock.Setup(x => x.Map<IEnumerable<PostViewModel>>(It.IsAny<IEnumerable<PostDto>>()))
                .Returns((IEnumerable<PostDto> source) =>
                {
                    return source.Select(s => new PostViewModel
                    {
                        Id = s.Id,
                        Content = s.Content
                    });
                });

            // Add action configuration

            _postsServiceMock.Setup(repo => repo.AddPost(It.IsAny<PostDto>())).Returns(Task.FromResult(new PostDto
            {
                Id = TestId
            }));
            _mapperMock.Setup(x => x.Map<PostDto>(It.IsAny<AddPostViewModel>()))
                .Returns((AddPostViewModel source) => new PostDto
                {
                    Content = source.Content
                });
            _mapperMock.Setup(x => x.Map<PostViewModel>(It.IsAny<PostDto>()))
                .Returns((PostDto source) => new PostViewModel
                {
                    Id = source.Id,
                    Content = source.Content
                });

            // Get action configuration

            _postsServiceMock.Setup(repo => repo.Get(It.IsAny<int>(), It.IsAny<bool>())).Returns(Task.FromResult(new PostDto
            {
                Id = TestId
            }));
        }

        private static IEnumerable<PostDto> GetTestPosts()
        {
            var posts = new List<PostDto>
            {
                new PostDto()
                {
                    Id = TestId,
                    Content = "Test 1"
                },
                new PostDto()
                {
                    Id = 1994,
                    Content = "Test 2"
                }
            };

            return posts;
        }

        [Fact]
        public async Task GetAll_ReturnsAViewResult_WithAListOfPosts()
        {
            var postsController = new PostsController(_postsServiceMock.Object, _mapperMock.Object);

            var result = await postsController.GetAll();

            var okResult = result.Should().BeOfType<OkObjectResult>().Subject;
            var posts = okResult.Value.Should().BeAssignableTo<IEnumerable<PostViewModel>>().Subject;

            posts.Count().Should().Be(2);
        }
        
        [Fact]
        public async Task Get_ReturnsSpecificPost_ById()
        {
            var postsController = new PostsController(_postsServiceMock.Object, _mapperMock.Object);

            var result = await postsController.Get(TestId);

            var okResult = result.Should().BeOfType<OkObjectResult>().Subject;
            var post = okResult.Value.Should().BeAssignableTo<PostViewModel>().Subject;
            post.Id.Should().Be(TestId);
        }

        [Fact]
        public async Task Add_ShouldAddNewPost()
        {
            var postsController = new PostsController(_postsServiceMock.Object, _mapperMock.Object);
            var newPost = new AddPostViewModel
            {
                Content = "Test Content"
            };

            var result = await postsController.Add(newPost);

            var okResult = result.Should().BeOfType<CreatedAtActionResult>().Subject;
            var post = okResult.Value.Should().BeAssignableTo<PostViewModel>().Subject;
            post.Id.Should().Be(TestId);
        }
    }
}

﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Travix.Api.Controllers;
using Travix.Api.ViewModels;
using Travix.BusinessComponents.Interfaces.Public;
using Travix.BusinessEntities.DtoModels;
using Xunit;

namespace Travix.Tests.Controllers
{
    public class CommentsControllerTest
    {
        private readonly Mock<ICommentsService> _commentsServiceMock = new Mock<ICommentsService>();
        private readonly Mock<IMapper> _mapperMock = new Mock<IMapper>();
        private const int TestId = 1993;

        public CommentsControllerTest()
        {
            // GetAll action configurartion

            _commentsServiceMock.Setup(repo => repo.GetAllComments()).Returns(Task.FromResult(GetTestComments()));
            _mapperMock.Setup(x => x.Map<IEnumerable<CommentViewModel>>(It.IsAny<IEnumerable<CommentDto>>()))
                .Returns((IEnumerable<CommentDto> source) =>
                {
                    return source.Select(s => new CommentViewModel
                    {
                        Id = s.Id,
                        Content = s.Content
                    });
                });

            // Add action configuration

            _commentsServiceMock.Setup(repo => repo.AddComment(It.IsAny<AddCommentDto>()))
                .Returns(Task.FromResult(new CommentDto
                {
                    Id = 2222,
                    Post = new PostDto
                    {
                        Id = TestId
                    }
                }));

            _mapperMock.Setup(x => x.Map<CommentDto>(It.IsAny<AddCommentViewModel>()))
                .Returns((AddCommentViewModel source) => new CommentDto
                {
                    Content = source.Content,
                    Post = new PostDto
                    {
                        Id = source.PostId
                    }
                });
            _mapperMock.Setup(x => x.Map<CommentViewModel>(It.IsAny<CommentDto>()))
                .Returns((CommentDto source) => new CommentViewModel
                {
                    Id = source.Id,
                    Post = new PostViewModel
                    {
                        Id = source.Post.Id,
                    },
                    Content = source.Content
                });

            // Get action configuration

            _commentsServiceMock.Setup(repo => repo.Get(It.Is<int>(e => e == 1993), It.IsAny<int>())).Returns(Task.FromResult(new CommentDto
            {
                Id = 222,
                Post = new PostDto
                {
                    Id = TestId
                }
            }));
        }

        private static IEnumerable<CommentDto> GetTestComments()
        {
            var posts = new List<CommentDto>
            {
                new CommentDto()
                {
                    Id = TestId,
                    Content = "Test Comment 1"
                },
                new CommentDto()
                {
                    Id = 1994,
                    Content = "Test Comment 2"
                }
            };

            return posts;
        }

        [Fact]
        public async Task GetAll_ReturnsAViewResult_WithAListOfComments()
        {
            var commentsController = new CommentsController(_commentsServiceMock.Object, _mapperMock.Object);

            var result = await commentsController.GetAll();

            var okResult = result.Should().BeOfType<OkObjectResult>().Subject;
            var comments = okResult.Value.Should().BeAssignableTo<IEnumerable<CommentViewModel>>().Subject;

            comments.Count().Should().Be(2);
        }

        [Fact]
        public async Task Add_ShouldAddNewCommentToPostWithSpecificId()
        {
            var commentsController = new CommentsController(_commentsServiceMock.Object, _mapperMock.Object);
            var newComment = new AddCommentViewModel
            {
                PostId = TestId,
                Content = "Test Content"
            };

            var result = await commentsController.Add(newComment);

            var okResult = result.Should().BeOfType<CreatedAtActionResult>().Subject;
            var comment = okResult.Value.Should().BeAssignableTo<CommentViewModel>().Subject;
            comment.Post.Id.Should().Be(TestId);
        }

        [Fact]
        public async Task Get_ReturnsSpecificComment_ByPostIdAndCommentId()
        {
            var commentsController = new CommentsController(_commentsServiceMock.Object, _mapperMock.Object);

            var result = await commentsController.Get(TestId, 111);

            var okResult = result.Should().BeOfType<OkObjectResult>().Subject;
            var comment = okResult.Value.Should().BeAssignableTo<CommentViewModel>().Subject;
            comment.Post.Id.Should().Be(TestId);
        }
    }
}

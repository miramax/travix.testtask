﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Travix.BusinessComponents.Interfaces.Database;
using Travix.BusinessEntities.DataContracts;

namespace Travix.DataAccess.Services
{
    /// <summary>
    /// Manages access to comments repository from database.
    /// </summary>
    /// <seealso cref="Travix.BusinessComponents.Interfaces.Database.ICommentsDatabaseService" />
    public class CommentsDatabaseService : ICommentsDatabaseService
    {
        private readonly ApplicationDbContext _applicationDbContext;

        public CommentsDatabaseService(ApplicationDbContext applicationDbContext)
        {
            _applicationDbContext = applicationDbContext;
        }

        /// <summary>
        /// Returns all comments.
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<Comment>> GetAll()
        {
            return await _applicationDbContext.Comments.ToListAsync();
        }

        /// <summary>
        /// Adds new comment.
        /// </summary>
        /// <param name="addComment">The add comment.</param>
        /// <returns></returns>
        public async Task<Comment> AddComment(AddComment addComment)
        {
            var comment = new Comment
            {
                PostId = addComment.PostId,
                Content = addComment.Content
            };
            _applicationDbContext.Comments.Add(comment);
            await _applicationDbContext.SaveChangesAsync();

            return comment;
        }

        /// <summary>
        /// Returns specific comment by postId and commentId.
        /// </summary>
        /// <param name="postId">The post identifier.</param>
        /// <param name="commentId">The comment identifier.</param>
        /// <returns></returns>
        public async Task<Comment> Get(int postId, int commentId)
        {
            var comment = await _applicationDbContext.Comments.SingleOrDefaultAsync(p => p.Id == commentId && p.PostId == postId);

            return comment;
        }
    }
}

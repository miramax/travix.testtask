﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Travix.BusinessComponents.Interfaces.Database;
using Travix.BusinessEntities.DataContracts;

namespace Travix.DataAccess.Services
{
    /// <summary>
    /// Manages access to posts repository from database.
    /// </summary>
    /// <seealso cref="Travix.BusinessComponents.Interfaces.Database.IPostsDatabaseService" />
    public class PostsDatabaseService : IPostsDatabaseService
    {
        private readonly ApplicationDbContext _applicationDbContext;

        public PostsDatabaseService(ApplicationDbContext applicationDbContext)
        {
            _applicationDbContext = applicationDbContext;
        }

        /// <summary>
        /// Returns all posts.
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<Post>> GetAll()
        {
            return await _applicationDbContext.Posts.ToListAsync();
        }

        /// <summary>
        /// Adds new post.
        /// </summary>
        /// <param name="post">The post.</param>
        /// <returns></returns>
        public async Task<Post> AddPost(Post post)
        {
            _applicationDbContext.Posts.Add(post);
            await _applicationDbContext.SaveChangesAsync();

            return post;
        }

        /// <summary>
        /// Returns the specific post by id.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="withComments">if set to <c>true</c> [with comments].</param>
        /// <returns></returns>
        public async Task<Post> Get(int id, bool withComments)
        {
            var post = await _applicationDbContext.Posts.SingleOrDefaultAsync(p => p.Id == id);

            if (withComments)
            {
                _applicationDbContext.Entry(post).Collection( p => p.Comments).Load();
            }

            return post;
        }
    }
}

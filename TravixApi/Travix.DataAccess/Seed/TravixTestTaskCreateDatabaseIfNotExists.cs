﻿using System.Collections.Generic;
using Travix.DataAccess.Seed.SqlCommands;

namespace Travix.DataAccess.Seed
{
    /// <summary>
    /// Creates new database if there is no one.
    /// </summary>
    public class TravixTestTaskCreateDatabaseIfNotExists
    {
        private readonly IEnumerable<ISqlCommand> _commands;

        public TravixTestTaskCreateDatabaseIfNotExists(params ISqlCommand[] commands)
        {
            _commands = commands;
        }
        /// <summary>
        /// Runs seed-commands.
        /// </summary>
        /// <param name="context">The context.</param>
        public void Seed(ApplicationDbContext context)
        {
            foreach (var command in _commands)
                command.Execute(context);
        }
    }
}

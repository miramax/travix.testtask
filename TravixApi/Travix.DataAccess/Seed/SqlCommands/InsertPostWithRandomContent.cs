﻿using System;
using System.Linq;
using Travix.BusinessEntities.DataContracts;

namespace Travix.DataAccess.Seed.SqlCommands
{
    /// <summary>
    /// Database configuration.
    /// </summary>
    /// <seealso cref="Travix.DataAccess.Seed.SqlCommands.ISqlCommand" />
    public class InsertPostWithRandomContent : ISqlCommand
    {
        private const int MinValue = 1;
        private const int MaxValue = 13;
        private string GenerateRandomContent()
        {
            var random = new Random();
            var randNumber = random.Next(MinValue, MaxValue);

            return String.Format("This is random number: {0}", randNumber);
        }
        /// <summary>
        /// Adds post to database with random content.
        /// </summary>
        /// <param name="context">The context.</param>
        public void Execute(ApplicationDbContext context)
        {
            if (context.Posts.Any()) return;

            var content = GenerateRandomContent();

            context.Posts.Add(new Post
            {
                Content = content
            });

            context.SaveChanges();
        }
    }
}

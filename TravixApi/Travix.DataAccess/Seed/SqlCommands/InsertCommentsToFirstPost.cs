﻿using System.Collections.Generic;
using System.Linq;
using Travix.BusinessEntities.DataContracts;

namespace Travix.DataAccess.Seed.SqlCommands
{
    /// <summary>
    /// Database configuration.
    /// </summary>
    /// <seealso cref="Travix.DataAccess.Seed.SqlCommands.ISqlCommand" />
    public class InsertCommentsToFirstPost : ISqlCommand
    {
        private readonly List<Comment> _commentList =
            new List<Comment>
            {
                new Comment
                {
                    Content = "Comment 1"
                },
                new Comment
                {
                    Content = "Comment 2"
                }
            };

        /// <summary>
        /// Adds comments to the first existed post.
        /// </summary>
        /// <param name="context">The context.</param>
        public void Execute(ApplicationDbContext context)
        {
            if (context.Comments.Any()) return;

            var post = context.Posts.First();

            _commentList.ForEach(comment =>
            {
                comment.Post = post;
                context.Comments.Add(comment);
            });

            context.SaveChanges();
        }
    }
}

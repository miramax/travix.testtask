﻿namespace Travix.DataAccess.Seed.SqlCommands
{
    public interface ISqlCommand
    {
        void Execute(ApplicationDbContext context);
    }
}

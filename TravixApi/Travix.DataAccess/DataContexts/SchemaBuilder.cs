﻿using Microsoft.EntityFrameworkCore;
using Travix.BusinessEntities.DataContracts;

namespace Travix.DataAccess.DataContexts
{
    /// <summary>
    /// Builds database relationships.
    /// </summary>
    public class SchemaBuilder
    {
        /// <summary>
        /// Adds the SQL specific stuff.
        /// </summary>
        /// <param name="modelBuilder">The model builder.</param>
        public static void AddSqlSpecificStuff(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Comment>(entity =>
            {
                entity.HasOne(d => d.Post)
                    .WithMany(p => p.Comments)
                    .HasForeignKey(d => d.PostId);
            });
        }
    }
}

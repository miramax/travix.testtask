﻿using Travix.BusinessEntities.DataContracts;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Travix.DataAccess.DataContexts;
using Travix.BusinessComponents.Configuration;

namespace Travix.DataAccess
{

    /// <summary>
    /// Manages access to database
    /// </summary>
    /// <seealso cref="Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityDbContext{ApplicationUser}" />
    public sealed class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
            Database.EnsureCreated();
        }

        private readonly string _connectionString;

        public ApplicationDbContext(
            string connectionString)
        {
            _connectionString = connectionString;
            Database.EnsureCreated();
        }

        public ApplicationDbContext()
            : this(ConfigurationManager.ConnectionString ?? "Data Source=EPBYMINW4167;Initial Catalog=Traviix11;Integrated Security=True")
        {
        }

        public DbSet<Post> Posts { get; set; }
        public DbSet<Comment> Comments { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            SchemaBuilder.AddSqlSpecificStuff(modelBuilder);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(_connectionString);
        }
    }
}

﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Travix.BusinessComponents.Interfaces.Database;
using Travix.BusinessComponents.Interfaces.Public;
using Travix.BusinessEntities.DataContracts;
using Travix.BusinessEntities.DtoModels;
using Travix.BusinessEntities.Exceptions;

namespace Travix.BusinessComponents.Services
{
    public class CommentsService : ICommentsService
    {
        private readonly ICommentsDatabaseService _commentsDatabaseService;
        private readonly IMapper _mapper;
        public CommentsService(ICommentsDatabaseService commentsDatabaseService, IMapper mapper)
        {
            _commentsDatabaseService = commentsDatabaseService;
            _mapper = mapper;
        }
        public async Task<IEnumerable<CommentDto>> GetAllComments()
        {
            var comments = await _commentsDatabaseService.GetAll();

            var result = _mapper.Map<IEnumerable<CommentDto>>(comments);
            return result;
        }

        public async Task<CommentDto> AddComment(AddCommentDto commentDto)
        {
            if (!commentDto.Content.All(char.IsLetterOrDigit))
            {
                throw new MessageException("Bad content");
            }

            var addComment = _mapper.Map<AddComment>(commentDto);
            var commentDao = await _commentsDatabaseService.AddComment(addComment);
            var result = _mapper.Map<CommentDto>(commentDao);

            return result;
        }

        public async Task<CommentDto> Get(int postId, int commentId)
        {
            var comment = await _commentsDatabaseService.Get(postId, commentId);
            var result = _mapper.Map<CommentDto>(comment);

            return result;
        }
    }
}

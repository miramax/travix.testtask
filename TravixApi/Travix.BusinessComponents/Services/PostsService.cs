﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Travix.BusinessComponents.Interfaces.Database;
using Travix.BusinessComponents.Interfaces.Public;
using Travix.BusinessEntities.DataContracts;
using Travix.BusinessEntities.DtoModels;
using Travix.BusinessEntities.Exceptions;

namespace Travix.BusinessComponents.Services
{
    public class PostsService : IPostsService
    {
        private readonly IPostsDatabaseService _postsDatabaseService;
        private readonly IMapper _mapper;
        public PostsService(IPostsDatabaseService postsDatabaseService, IMapper mapper)
        {
            _postsDatabaseService = postsDatabaseService;
            _mapper = mapper;
        }
        

        public async Task<IEnumerable<PostDto>> GetAllPosts()
        {
            var posts = await _postsDatabaseService.GetAll();
            var result = _mapper.Map<IEnumerable<PostDto>>(posts);

            return result;
        }

        public async Task<PostDto> AddPost(PostDto post)
        {
            if (!post.Content.All(char.IsLetterOrDigit))
            {
                throw new MessageException("Bad content");
            }

            var postDao = _mapper.Map<Post>(post);

            var postResult = await _postsDatabaseService.AddPost(postDao);
            var postDtoResult = _mapper.Map<PostDto>(postResult);

            return postDtoResult;
        }

        public async Task<PostDto> Get(int id, bool withComments)
        {
            var post = await _postsDatabaseService.Get(id, withComments);
            var postDtoResult = _mapper.Map<PostDto>(post);

            return postDtoResult;
        }
    }
}

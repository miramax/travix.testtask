﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Travix.BusinessEntities.DtoModels;

namespace Travix.BusinessComponents.Interfaces.Public
{
    public interface IPostsService
    {
        Task<IEnumerable<PostDto>> GetAllPosts();
        Task<PostDto> AddPost(PostDto post);
        Task<PostDto> Get(int id, bool withComments);
    }
}

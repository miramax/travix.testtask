﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Travix.BusinessEntities.DtoModels;

namespace Travix.BusinessComponents.Interfaces.Public
{
    public interface ICommentsService
    {
        Task<IEnumerable<CommentDto>> GetAllComments();
        Task<CommentDto> AddComment(AddCommentDto post);
        Task<CommentDto> Get(int postId, int commentId);
    }
}

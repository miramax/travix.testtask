﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Travix.BusinessEntities.DataContracts;

namespace Travix.BusinessComponents.Interfaces.Database
{
    public interface ICommentsDatabaseService
    {
        Task<IEnumerable<Comment>> GetAll();
        Task<Comment> AddComment(AddComment comment);
        Task<Comment> Get(int postId, int commentId);
    }
}

﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Travix.BusinessEntities.DataContracts;

namespace Travix.BusinessComponents.Interfaces.Database
{
    public interface IPostsDatabaseService
    {
        Task<IEnumerable<Post>> GetAll();
        Task<Post> AddPost(Post post);
        Task<Post> Get(int id, bool withComments);
    }
}

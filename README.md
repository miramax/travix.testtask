### What is this repository for? ###

You need to create web service that allows interacting with such models: posts and comments (one to many).
•	Implement Restful API
•	Create at least 2 async endpoints for realizing CRUD operations (up to you which one) 
•	Realize multilayer architecture
•	Use IoC
•	Data validation
•	Cover code by tests

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact